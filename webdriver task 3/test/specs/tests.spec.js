class GoogleCloudHomePage {
    constructor() {
        this.searchIcon = $("//input[@placeholder='Search']");
    }

    async navigateTo() {
        await browser.navigateTo("https://cloud.google.com/");
        await browser.pause(1000);
    }

    async searchFor() {
        const searchText = "Google Cloud Platform Pricing Calculator";
        await this.searchIcon.click();
        await this.searchIcon.addValue(searchText);
        await browser.keys('Enter');
        await browser.pause(1000);
    }
}

class PricingCalculatorPage {
    constructor() {
        this.googleCloudPricingCalculatorLink = $("//b[text()='Google Cloud Pricing Calculator']");
        this.addEstimateButton = $("//span[@class='AeBiU-kBDsod-Rtc0Jf AeBiU-kBDsod-Rtc0Jf-OWXEXe-M1Soyc']//i[text()='add']");
        this.computeEngineOption = $("//h2[text()='Compute Engine']");
    }

    async clickGoogleCloudPricingCalculatorLink() {
        await this.googleCloudPricingCalculatorLink.click();
        await browser.pause(1000);
    }

    async clickAddEstimateButton() {
        await this.addEstimateButton.click();
        await browser.pause(1000);
    }

    async clickComputeEngineOption() {
        await this.computeEngineOption.click();
        await browser.pause(1000);
    }
}

class ComputeEngineSection {
    constructor() {
        this.addInstanceButton = $("//button[@aria-label='Increment']");
        this.machineTypeDropdown = $("//div[@jsname='wSASue']//div[@class='VfPpkd-xl07Ob-XxIAqe VfPpkd-xl07Ob-XxIAqe-OWXEXe-tsQazb VfPpkd-xl07Ob VfPpkd-YPmvEd IWDrLe']");
        this.machineTypeOption = $("//li[@data-value='n1-standard-8']");
        this.addGpuButton = $("//div[@class='kqQzpb P33MY']//button[@type='button']");
        this.gpuTypeButton = $("//div[@class='VfPpkd-O1htCb VfPpkd-O1htCb-OWXEXe-INsAgc VfPpkd-O1htCb-OWXEXe-SfQLQb-M1Soyc-Bz112c FkS5nd VfPpkd-O1htCb-OWXEXe-XpnDCe']//span[@class='VfPpkd-t08AT-Bz112c']");
    }

    async clickAddInstanceButton() {
        await this.addInstanceButton.click();
        await browser.pause(5000);
    }

    async selectMachineType() {
        await this.machineTypeDropdown.click();
        await this.machineTypeOption.click();
        await browser.pause(5000);
    }

    async clickAddGpuButton() {
        await this.addGpuButton.click();
        await browser.pause(1000);
    }

    async selectGpuType() {
        await this.gpuTypeButton.click();
        await browser.pause(1000);
    }
}

class InitialSteps {
    constructor() {
        this.googleCloudHomePage = new GoogleCloudHomePage();
        this.pricingCalculatorPage = new PricingCalculatorPage();
    }

    async performInitialSteps() {
        await this.googleCloudHomePage.navigateTo();
        await this.googleCloudHomePage.searchFor("Google Cloud Platform Pricing Calculator");
        await this.pricingCalculatorPage.clickGoogleCloudPricingCalculatorLink();
        await this.pricingCalculatorPage.clickAddEstimateButton();
        await this.pricingCalculatorPage.clickComputeEngineOption();
    }
}

class RemainingSteps {
    constructor() {
        this.computeEngineSection = new ComputeEngineSection();
    }

    async performRemainingSteps() {
        await this.computeEngineSection.clickAddInstanceButton();
        await this.computeEngineSection.selectMachineType();
        await this.computeEngineSection.clickAddGpuButton();
        await this.computeEngineSection.selectGpuType();
    }

    async takeScreenshot() {
        const dateTime = new Date().toISOString().replace(/:/g, "-");
        await browser.saveScreenshot(`failure-${dateTime}.png`);
    }
}

describe("Test suite for https://cloud.google.com/ website", () => {
    it("First test suite", async () => {
        const initialSteps = new InitialSteps();
        await initialSteps.performInitialSteps();

        const remainingSteps = new RemainingSteps();
        try {
            await remainingSteps.performRemainingSteps();
        } catch (error) {
            await remainingSteps.takeScreenshot();
            throw error;
        }
    });
});

